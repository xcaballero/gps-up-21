# Erasmus UPC - PLA D'ITERACIÓ *Inception* #

## 1. PRESENTACIÓ DE LA ITERACIÓ ##

En la fase "d'Inception" un escàs 5% del total d'hores del projecte hi són invertides, el que en xifres absolutes dona un resultat de 107 hores. 
D'aquestes 107 hores, l'analista de software és, amb diferència, qui més esforç hi dedica durant aquesta fase. En termes relatius parlem d'un 65% del total, el que ve a ser quasi 70 hores; seguidament tenim al gestor de projectes, que haurà d'invertir 21.4 hores (un 20%) i darrerament l'Arquitecte (10%) i l'Analista de programació (5%), que apenes sumen 16 hores entre els dos.
Els programadors i testers en aquesta fase no seràn requerits per ninguna tasca.
Com que la realització d'aquesta fase es farà en una única iteració, podem concloure que, segons les hores estipulades en el calcul d'Schedul, la data de finalització d'aquesta iteració serà 5 setmanes i dos dies després del día d'inici (9 de Gener - 14 de Febrer).

## 2. COBERTURA DE CASOS D'ÚS ##
![](https://s15.postimg.org/i57fm6ypn/Captura_de_pantalla_2016_11_04_16_03_09.png)

## 3. ACTIVITATS ##

###Gestió projecte

#### A01 - Reclutar personal ####

Temps: 7 dies dies.

Precedències: A06

#### A02 - Definir processos de control ####

Temps: 5 dies dies.

Precedències: A01

#### A03 - Programar plans d'iteració ####

Temps: 5 dies dies.

Precedències: A02


### Negoci

#### A04 - Evaluar l'estat del negoci ####

Temps: 1 dia.

Precedències: No hi ha precedències

#### A05 - Definir impacte econòmic del projecte ####

Temps: 1 dia.

Precedències: A04

#### A06 - Definir rols i responsabilitats ####

Temps: 2 dies.

Precedències: A05

### Requisits
#### A07 - Desenvolupar la visió ####

Temps: 2 dies dies.

Precedències: A08, A09

#### A08 - Definir requeriments no funcionals  #### 

Temps: 2 dies dies.

Precedències: A01

#### A09 - Identificar requisits  ####

Temps: 2 dies dies.

Precedències: A01

#### A10 - Identificar casos d'ús  ####

Temps: 5 dies dies.

Precedències: A07

#### A11 - Identificar actors del sistema  ####

Temps: 5 dies dies.

Precedències: A07


### Anàlisi i disseny
#### A12 - Anàlisi arquitectura ####

Temps: 7 dies dies.

Precedències: A07
 
#### A13 - Disseny cas d’ús ####

Temps: 3 dies dies.

Precedències: A10, A11


### Entorn ###
#### A14 - Configurar eines ####

Temps:  2 dies.

Precedències: A07

#### A15 - Desenvolupar guies de proves ####

Temps: 2 dies.

Precedències: A13
 
## 4. DIAGRAMA DE PERT I GANTT ##
Aqui podem veure el diagrama de Pert

![](https://s12.postimg.org/xdx8782wt/diagrama_de_pert_1.png)

Aqui podem veure el diagrama de GANTT


![](https://s11.postimg.org/4h4wav8mr/Captura_de_pantalla_2016_11_04_17_59_01.png)
