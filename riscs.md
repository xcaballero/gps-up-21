# Erasmus UPC - RISCS #

## RISC 001. Poc impacte social ##

### Descripció ###

L'impacte en l'estudiantat, tant autòcton com internacional, no s'adecua al previst tot i els estudis realitzats i la necessitat d'aquest sistema.

### Probabilitat ###

Poc provable.
 
### Impacte ###

L'impacte d'aquest risc provocarà una funcionalitat en desús o inútil per la universitat.
 
### Indicadors ###

Un indicador clar que ha ocurregut aquest risc és que hi hagi poca descàrrega del sistema i/o poc ús del mateix. 

De la mateixa manera els comentaris que hi pugui haver en els diferents distribuidors d'aplicacions serà un idicador de la situació actual del producte. 
 
### Estratègies de mitigació ###

S'ha d'explicar bé l'utilitat de l'aplicació a l'estudiantat quan arriba a la universitat mostrant els beneficis del seu ús.
 
### Plans de mitigació ###

Una mesura per reduir l'impacte d'aquest risc és afegir un espai on l'estudiantat pugui dir quins són els motius pels quals no fa ús de la mateixa.

## RISC 002. Excedir el pressuopost o el temps. ##


### Descripció ###

Exedir el pressupost o el temps de finalització de l'apliacació a causa d'una mala planificació inicial.

### Probabilitat ###

Versemblant
 
### Impacte ###

L'impacte d'aquest risc seria un sobrecàrrec per part de la universitat i/o l'espera d'un quadrimestre per donar-li ús a causa que els estudiants internacionals ja estiguin establerts a la ciutat i a la universitat.
 
### Indicadors ###

No complir els plaços inicials temporitzats, trobar errors de planificació que s'hagin de corregir a mig desenvolupament i per tant comportin una pèrdua de temps i de recursos.
 
### Estratègies de mitigació ###

Fer una bona planificació del projecte, contractar a personal amb experiència i ser realista.
 
### Plans de mitigació ###

Contractar més personal si el pressupost és suficient.

## RISC 003. Intrusió de seguretat ##


### Descripció ###

Que hi hagi atacs a la nostra apliació que vulnerin la privacitat de les dades o bloquegin el sistema.

### Probabilitat ###

Probable.
 
### Impacte ###

Perdua de confiança de l'estudiantat i en conseqüència desús de l'aplicatiu.
 
### Indicadors ###

Visualització de problemes en el funcionament de l'aplicació, recull de logs que surten de la norma o advertències dels tallafocs.
 
### Estratègies de mitigació ###

Utilitzar servidors segurs i tenir en compte la seguretat alhora de planificar i desenvolupar l'aplicació.
 
### Plans de mitigació ###

Demanar una certificació a l'empresa externa de servidors que confirmi la seguretat de les dades.


## RISC 004 Problemes de compatibilitat ##


### Descripció ###

L'aplicació no és compatible amb tots els sistemes operatius actuals.

### Probabilitat ###

Poc probable.
 
### Impacte ###

Nombre d'usuaris que fan ús de l'aplicació molt inferior a l'esperat.
 
### Indicadors ###

Provar l'execució en diversos sistemes i veure que n'hi ha que no en fan una execució correcte.
 
### Estratègies de mitigació ###

Una estratègia és desenvolupar l'apliació en una versió antiga del sistema operatiu per tal d'assegurar que pugui ser d'ús en totes les versions.
 
### Plans de mitigació ###

Ampliar el rang de dispositius que poden fer ús de la nostra apliació.
 

## RISC 005. Errors en el sistema publicat ##


### Descripció ###

Un cop l'aplicació esta al mercat te comportaments erronis.

### Probabilitat ###

Versemblant.
 
### Impacte ###

Si l'aplicació te errors quan es fa la primera publicació o en futures versions i no s'actua rapidament farà perdre la confiança de l'usuari en la mateixa.
 
### Indicadors ###

Els usuaris publiquen errors a pàgines de la universitat o bé ho comenten als mercats d'aplicacions dels diferents dispositius.
 
### Estratègies de mitigació ###

Realitzar testing suficient per assegurar el funcionament de l'aplicació.
 
### Plans de mitigació ###

Estar pendent dels comentaris de la comunitat i solucionar els possibles problemes de manera ràpida i efectiva.

## RISC 006. Baixa disponibilitat ##


### Descripció ###

A causa de problemes amb els servidos el sistema deixa de funcionar.

### Probabilitat ###

Poc probable.
 
### Impacte ###

Perdua d'informació en cas de no estar ben planificat el desenvolupament.

Perdua d'interès o confiança de l'usuari.
 
### Indicadors ###

L'aplicació no retorna resultats de les consultes a les bases de dades.
 
### Estratègies de mitigació ###

Fer servir un sistema de backups per si hi ha problemes futurs.
 
### Plans de mitigació ###

Augmentar el número de servidors contractats o bé canviar de companyia.


## RISC 007. Canvis en els requisits ##


### Descripció ###

A mig desenvolupament o bé un cop ja publicada l'aplicació hi ha un canvi de requisits en el sistema.

### Probabilitat ###

Poc probable.
 
### Impacte ###

En cas de no estar ben dissenyada l'aplicació caldria una gran aportació econòmica i temporal per aplicar els canvis.

En cas d'estar ben dissenyada però hi hagi uns nous requisits que no tenen res a veure amb els actuals suposaria una gran inversió.
 
### Indicadors ###

Canvi directiu de la universitat o redistribució de carregs interns.
 
### Estratègies de mitigació ###

Consensuar el sistema amb els diferents stakeholders de la universitat.

Disenyar una apliació modular de tal manera que els petits canvis siguin fàcils d'aplicar.
 
### Plans de mitigació ###

Aprofitar tot el codi actual possible i intentar que els canvis afectin al menor número d'arees de l'aplicatiu.