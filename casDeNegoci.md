
# Erasmus UPC - CAS DE NEGOCI #

## 1. DESCRIPCIÓ DEL PRODUCTE ##

El producte preten ajudar als nous estudiants a la universitat, gestionant els horaris de classe, informar del funcionament de cadascuna de les assignatures, donar informació dels llocs més visitats per els propis estudiants, buscar pis, entre altres. En resum la idea es unificar tota la informació que un nou estudiant necessitaria.

## 2. CONTEXT DE NEGOCI ##

El producte pertany al sector public ja que es un projecte demanat per la UPC. 
És un producte de consum intern, en particular de la UPC, per tant s'intentarà aconseguir un màxim de benefici social, amb el qual es podrà aconseguir algún benefici gràcies a les matricules d'aquests. 
A més aprofitant que el projecte es per la UPC podem posarnos en contacte amb altres institucions que els pertany. *SALSA'M* i el *Servei d'Esports UPC*. Tots aquests grups podrien estar inclosos a la part d'oci de la aplicació per davant de les altres possibilitats generant aixi un benefici econòmic i un molt bon producte per la universitat. Afegir també que la UPC rep un gran nombre d'estudiants d'erasmus que amb el producte milloraria el servei que dóna.
Pel que fa al desenvolupament, la aplicació tindrà moltes funcionalitats a implementar a més hi ha una part molt important d'entrevistat amb els estudiants actuals de la universitat, ja que la informació a incloure a l'aplicació vindrà directament d'ells. Per tot això, un necessitariem entre 1 i 2 programadors per cada funcionalitat/sector de l'aplicació, i altres empleats que es dediquin a recopilar informació 2-3 persones repartides per cadascun dels campus depenent dels diners que si volen gastar, i per últim s'hauria d'anunciar una mica l'aplicació però bastaria anunciantse en els llocs oficials de la UPC.

## 3. OBJECTIUS DEL PRODUCTE ##

1. *Orientació*. Donar informació de les tarifes del transport públic. 
2. *Unificació*. Volem que l'estudiant tengui a mà tota la informació acadèmica que necessiti. 
3. *Integració*. Anirem publicant la informació de tots els events relacionats amb les universitats(festes i farres universitaries)
4. *Cerca d'allotjament*. Donarem informació bàsica de les residències i agències, per tal d'ajudar a trobar allotjament. A més l'aplicació dispondrà d'un xat o fórum on podran buscar company de pis.
5. *Guía turistica*.
6. *Recomenacions*. Ja siguin de llocs on menjar, o bé llocs per divertir-se
7. *Ensenyar la nostre cultura*. SALSA'M ens podria ajudar amb aquesta tasca, ja que l'aplicació només podria donar informació de les festes de barri... 

## 4. RESTRICCIONS ##

1. *Components equip:*. L'equip de programació haura de ser de com a mínim 6 persones.
2. *Domini:*. L'aplicació esta dirigida a només estudiants de la UPC que realitzen l'estada internacional i els mentors d'aqeusts.
3. *Durada:*. És necessari que l'aplicació estigui disponible pel quadrimestre de primavera del curs 2017-2018.
4. *Restaurants propers:*. Com a mínim ha de sortir els llocs on poder menajr més propers desde la universitat.
5. *Plataformes:*. S'ha d'implementar a tots els dispositius mobils moderns.
6. *Velocitat:*. És importantissim que l'acces a les dades sigui òptim.


## 5. PREVISIÓ FINANCERA ##

El benefici social de l'aplicació anirà lligat amb la utilització d'aquesta. S'espera que la UPC recomani la nostra eina als estudiants estragers per tant si l'aplicació esta ben mondada i és útil tindrà un benefici social molt elevat per aquests alumnes que no dubtaràn en recomenar l'aplicació.

## 6. RECURSOS ##

[*SALSA'M*] (https://www.upc.edu/slt/ca/acollida/salsam)

[*Servei d'Esports UPC*] (http://www.upc.edu/esports)
