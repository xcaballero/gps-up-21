# Erasmus UPC - VISIÓ #


## 1. INTRODUCCIÓ ##

La UPC, per continuar essent un reclam a nivell internacioanl i engreixar el seu fluxe d'intercanvis d'estudiants, ha apostat per la creació d’un projecte software que ajudi als estrangers que venen a integrar-se en la facultat i tenir una ràpida adaptació a la nostra vida universitaria. El sistema, que serà una aplicació mòbil descarregable de foma gratuïta, aspira ésser l'eina essencial de supervivència pels nouvinguts. Per fer-ho, centrarem la nostra atenció a brindar ajut i informació en aquelles dinàmiques típiques detectades de qualssevol erasmus: alternatives de mobilitat interurbana, opcions habitacionals, oci a la ciutat, programes de sociabilització amb altres erasmus, informació administrativa, gestions acadèmiques, mapes del campus... tot en la major comoditat visual i lingüística per l'usuari.


## 2. EL PROBLEMA ##

L'intercanvi d'alumnes mitjançant els programes de mobilitat és un pràctica molt recomanada i exercida per milers d'estudiants d'arreu del món. Tot i així, aquests, abans de marxar i durant el procès de transició, s'enfronten a una serie d'obstacles que, en ocasions, dificulten la seva adaptació i integració.
En molts casos es tracta de la primera experiència de residencia fora del país d'origen, així doncs, és important que l'estada sigui comfortable i la recepció adequada.
En un altre aspecte, les universitats tenen una gran competència per guanyar-se el prestigi i el seu reconeixament internacional. Això significa que pren una gran importancia ser un referent d'acollida d'estudiants d'arreu del mòn pel seu posicionament i la seva solvència econòmica.
I per últim, en aquesta mateixa línea argumental, trobem que són també les pròpies ciutats les primeres interessades en esdevindre punts de d'aglutinament i recepció erasmus; pel moviment cultural i estimulació de les activitats economiques que això propicia. 

 
## 3. PARTS INTERESSADES ##

1. *E-Secretaria*. 
Aconseguirà fer més accessibles i àgils les tràmitacions administratives en linea, donant major autonomia als usuaris recent arribats. 
Haurà d'atendre totes les peticions que rebin del sistema per al seu bon funcionament.

2. *Salsa'm*. 
Tindrà un gran proximitat i conectivitat amb els alumnes d'acollida. 
El seu rol al sistema és el de donar mentoría als usuaris del sistema que ho demanin.

3. *Estudiant d'intercanvi*. 
És el màxim beneficiari de totes les funcionalitats del sistema.

4. Desenvolupadors. 
Els treballadors del projecte, al ser remunerats per les hores laborals invertides a la construcció del projecte són una part interessada. 
Són els responsables d'acabar el sistema abans de la data d'entrega amb totes les funcionalitats operatives i testejades.

5. Rectorat. 
El rector en augmentar el nombre de sol·licituds erasmus i expandir el prestigi de la universitat. 
La seva responsabilitat és també primordial, haurà d'assegurar l'inversió dels recursos necessaris per a que surti endavant el projecte.

6. Negocis circumdants. 
Voldràn publicitar-se en una plataforma plena de potencials compradors.

7. Altres universitats
Com a competencia es veuen afectats pels serveis donats per la UPC, i per tant, susciten el seu interés.

8. Hosting
Interés: Vendre el seu servei.
Responsabilitat: cumplir amb els requeriments del servei demandat.


## 4. EL PRODUCTE ##

Aquest sistema en concret es tracta d'una aplicació mòbil en la que és necessari un registre d'usuari mitjançant les seves credencials d'estudiant a la UPC. D'aquesta manera, l'usuari dispondrà d'una àrea personal, on podrà fer totes aquelles consultes relacionades amb el seu perfil acadèmic: Horari, enllaços a les assignatures que estigui cursant, expedient, accés a les funcionalitats de la e-secretaria (sol·licituds, tràmits, matrícula...), etc.
Per a ajudar a la cerca d'habitatge a la ciutat mostrarem les principals residències de l'entorn universitari i altres alternatives a les que poden acudir (pàgines d'interès, inmobiliaries, etc).
Un altre eix fonamental i molt lligat a agilitzar l'adaptació del erasmus són les activitats de sociabilització, impulsades desde programes d'integració com "Salsa'm", que estaràn conectats al nostre sistema.

   

### 4.1. Perspectiva del producte ###

Estem parlant d'una aplicació mòbil gratuïta amb requeriments de conectivitat per accedir a certes funcionalitats del sistema i amb restriccions en el registre dels usuaris (només podràn aquells estudiants que estiguin dins del programa d'intercanvi d'erasmus). El servei anirà dirigit i centrat a aquests estudiants extrangers i per tant serà multilingüe. 


![](https://s13.postimg.org/tucksgibb/prespectiva_de_producte.jpg)

### 4.2. Descripció del producte ###

1. *Log In*. L'usuari podrà accedir al seu espai personal.
2. *Gestió acadèmica*. Totes les operacions de e-secretaria estaràn disponibles.
3. *Consulta de pisos*. Es volcarà informació sobre les ofertes de pis publicades.
4. *Mobilitat*. Es publicarà informació sobre el transport públic.
5. *Mentoría*. Hi haurà un canal de comunicació entre l'erasmus i el seu mentor. 
6. *Oci*. Es publicarà informació sobre activitats lúdiques a la ciutat.
 
### 4.3. Supòsits de funcionament ###

1. *Conexió a internet*. Necessitat d'una conexió a internet per tenir accés a totes les funcionalitats del sistema.
2. *Smartphone*. Adquisició de tecnología mòbil suficient per al sistema.
 
### 4.4. Dependències sobre altres sistemes ###

1. *E-secretaria*.
2. *SALSA'M*
3. *Servei d'Esports UPC*
  
### 4.5. Altres requisits ###

1. *Usabilitat*. 
No necessita de preparació tècnica prèvia per part de l'usuari, una dinàmica d'ús senzilla...
2. *Accessibilitat*. 
El sistem ha d'estar a l'abast tecnològic i econòmic de qualssevol estudiant d'intercanvi que tingui un Smartphone. 
3. *Disponibilitat*. 
El sistema ha d'estar disponible a qualssevol moment del día.
4. *Capacitat*. 
El sistema ha de poder processar totes les sol·licituds simultànies que generin varis usuaris alhora sense fallar.
5. *Privacitat*. 
El sistema ha de cumplir amb la legislació vigent de protecció de dades.

## 5. RECURSOS ##

[*SALSA'M*] (https://www.upc.edu/slt/ca/acollida/salsam)

[*Servei d'Esports UPC*] (http://www.upc.edu/esports)

[*E-Secretaria*] (https://esecretaria.upc.edu)
