# Erasmus UPC - ESPECIFICACIÓ DE REQUISITS DEL SOFTWARE #

## 1. ESPECIFICACIÓ FUNCIONAL ##

### 1.1. Diagrama de casos d'ús

![](https://s11.postimg.org/txx7zdr8z/Untitled_Diagram.png)

- Cas d'ús UC001 - *Consultar Servei d'Esports*: Permet a l'usuari veure l'última informació que envia el Servei d'Esports.

- Cas d'ús UC002 - *Log In*: Permet a l'usuari iniciar la sessió a l'aplicatiu. 

- Cas d'ús UC003 - *Consultar TMB*: Permet a l'usuari accedir a informació dels mitjans de transport públic de la ciutat.

- Cas d'ús UC004 - *Consultar informació d'interès*: Permet a l'usuari consultar un mapa del campus i zones properes on podrà veure diversa informació d'interès.

- Cas d'ús UC005 - *Consultar residències*: Mostrarà a l'usuari un llistat de l'informació de contacte de diferents residències d'estudiants properes al campus.

- Cas d'ús UC006 - *Tancar sessió*: Permet a l'usuari tancar la seva sessió a l'aplicatiu.

- Cas d'ús UC007 - *Recuperar contrasenya*: Permet a l'usuari iniciar un procés de recuperació de contrasenyes.

- Cas d'ús UC008 - *Consultar informació personal*: Permet a l'usuari veure la seva informació personal i la del seu mentor. O viceversa.

- Cas d'ús UC009 - *Consultar horari*: L'usuari podrà consultar el seu horari o el del seu alumne associat.

- Cas d'ús UC010 - *Consultar informació acadèmica*: Donarà accés a l'usuari a la part interactuable de la e-secretaria de la UPC.

- Cas d'ús UC011 - *Consultar guia docent*: L'usuari podrà accedir a la guia docent de l'assignatura seleccionada.

- Cas d'ús UC012 - *Modificar la configuració*: Permet a l'usuari canviar la configuració per defecte.

- Cas d'ús UC013 - *Gestió de comptes*: Permet a l'administrador gestionar els comptes de tots els estudiants.

- Cas d'ús UC014 - *Gestionar TMB*: Permet a l'administrador gestionar la informació visible de TMB pels usuaris.

- Cas d'ús UC015 - *Gestionar informació d'interès*: Permet a l'administrador gestionar la informació del mapa del campus.

- Cas d'ús UC016 - *Gestionar residències*: Permet a l'administrador gestionar la informació de les residències.

- Cas d'ús UC017 - *Gestió de la informació personal*: Permet a la gestió acadèmica gestionar la informació personal de tots els estudiants.

- Cas d'ús UC018 - *Gestió horari*: Permet a la gestió acadèmica gestionar l'horari de tots els usuaris.

- Cas d'ús UC019 - *Gestió de la informació acadèmica*: Permet a la gestió acadèmica gestionar la informació acadèmica dels estudiants.

- Cas d'ús UC020 - *Gestionar guia docent*: Permet a la gestió acadèmica gestiona la guia docent de les assignatures impartides pels estudiants.
 
 
### 1.2. Descripció individual dels casos d'ús

#### Cas d'ús UC001 - *Consultar Servei d'Esports* ####

L'usuari pot consultar la informació actualitzada de l'oferta esportiva del Servei d'Esports prement la secció anomenada. La oferta que tindrà disponible serà la que envia bimensualment el mateix Servei d'Esports.

#### Cas d'ús UC002 - *Log In* ####

L'usuari, que disposa de conexió a internet, introdueix les seves credencials per accedir al sistema de forma satisfactòria si previament ha estat enregistrat, o amb un error d'entrada en cas contrari.

#### Cas d'ús UC003 - *Consultar TMB* ####

L'usuari podrà consultar la informació actualitzada de les tarifes, linies i informació diversa relacionada amb el transport públic de la ciutat.

#### Cas d'ús UC004 - *Consultar informació d'interès* ####

L'usuari podrà consultar els diferents punts d'interès que hi ha tant al campus com a les zones més inmediates, ja sigui la situació geogràfica com telèfons i/o correus electronics del mateix.

#### Cas d'ús UC005 - *Consultar residències* ####

L'usuari podrà veure la informació de contacte de les diferents residències properes al campus al qual estudia.

#### Cas d'ús UC006 - *Tancar sessió* ####

L'usuari, que previament estarà "loguejat", podrà tancar la sessió del seu compte en qualsevol moment si així ho desitja.

#### Cas d'ús UC007 - *Recuperar contrasenya* ####

L'usuari podrà desencadenar una serie d'events per tal de poder recuperar o restaurar la contrasenya del seu compte, de la mateixa manera que ho pot fer actualment des de la web de la upc.

#### Cas d'ús UC008 - *Consultar informació personal* ####

L'usuari ha iniciat la seva sessió i el sistema li mostra a l'usuari una opció que li permet veure les dades del seu perfil, tals com la seva imatge.

#### Cas d'ús UC009 - *Consultar horari* ####

L'usuari podrà consultar el seu horari i, en cas d'estar al programa SALSA'M, el del seu mentor (en cas de ser alumne extranger) o alumne extranger assignat (en cas de ser el mentor).

#### Cas d'ús UC010 - *Consultar informació acadèmica* ####

L'usuari podrà consultar la seva informació acadèmica i fer tots aquells tàmits que actualment pot fer des de la e-secretaria des de la pròpia aplicació.

#### Cas d'ús UC011 - *Consultar guia docent* ####

L'usuari podrà consultar la guia docent de les assignatures que actualment està cursant.

#### Cas d'ús UC012 - *Modificar la configuració* ####

L'usuari, havent accedit satisfactòriament al sistema, dessitja modificar la configuració de la aplicació. El sistema mostra les diferents opcions, com per exemple l'idioma i ell podrà modificar el que li convingui.

#### Cas d'ús UC013 - *Gestió de comptes* ####

L'administrador podrà gestionar els comptes tant dels estudiants de la UPC com els extrangers que venen d'intercanvi. D'aquesta manera els comptes estaràn actualitzats en tot moment. L'administrador pot afegir, eliminar i modificar comptes.

#### Cas d'ús UC014 - *Gestionar TMB* ####

L'administrador podrà actualitzar la informació del transport públic poden afegir, eliminar i modificar informació en tot moment.

#### Cas d'ús UC015 - *Gestionar informació d'interès* ####

L'administrador podrà afegir, eliminar i modificar la informació d'interès que podrà consultar l'usuari.

#### Cas d'ús UC016 - *Gestionar residències* ####

L'administrador podrà afegir, eliminar i modificar la informació de residències properes als diferents campus de la UPC.

#### Cas d'ús UC017 - *Gestió de la informació personal* ####

L'administrador podrà afegir, eliminar i modificar la informació personal dels usuaris en cas que sigui necessari.

#### Cas d'ús UC018 - *Gestió horari* ####

L'administrador podrà fer modificacions en l'horari de l'alumne en cas que hi hagi canvis imprevistos després de la matriculació.

#### Cas d'ús UC019 - *Gestió de la informació acadèmica* ####

L'administrador serà l'encarregat de decidir quines funcions s'activaran per l'ús de l'ususari en funció de la necessitat del moment. Per exemple la funció d'automatrícula només ha d'estar activa en les dates indicades per a tal funció.

#### Cas d'ús UC020 - *Gestionar guia docent* ####

L'administrador podrà afegir, eliminar i modificar la informació de la guia docent de les diferents assignatures de la UPC.


## 2. ESPECIFICACIÓ NO FUNCIONAL ##

1. *Usabilitat*. ¿Identificador?
Descripció: El sistema haurà de ser fácil d'utilitzar pel nostre públic.
Justificació: La usabilitat del nostre sistema és important perque els usuaris no abandonin l'aplicació. 
Condició de satisfacció: El 90% dels usuaris continuaràn donant ús al nostre sistema dos setmanes després de descarregar-lo i realitzarem enquestes puntuals d'usabilitat.

2. *Accessibilitat*. 
Descripció: El sistema podrà ser utilitzat per qualssevol persona que disposi d'un smartphone senzill.
Justificació: Perque el sistema prengui sentit, haurà de ser de fàcil accés i estar a l'abast per qualssevol estudiant erasmus.
Condició de satisfacció: El nostre sistema serà gratuït i cumplirà amb els estàndards d'accessibilitat per qualssevol estudiant de la universitat.

3. *Disponibilitat*.
Descripció: El sistema estarà disponible les 24 hores del dia els 365 dies de l'any. 
Justificació: La completa disponibilitat del sistema que tractem és crucial perqué l'usuari pugui tramitar i consultar les seves sol·licituds qualssevol día a qualssevol hora.
Condició de satisfacció: Es contractarà un servei de hosting amb una disponibilitat total certificada.

4. *Capacitat*.
Descripció: El sistema ha de poder processar totes les sol·licituds simultànies que generin varis usuaris sense fallar.
Justificació: No ens podem permetre tenir caigudes del sistema en quant l'aplicació tingui èxit entre els estudiants.
Condició de satisfacció: Testejarem el sistema per a que sigui capaç de processar més de 100 sol·licituds per segon sense problemes.

5. *Privacitat*.
Descripció:  El sistema ha de cumplir amb la legislació vigent de protecció de dades.
Justificació: Es tracta d'una imposició legislativa que hem de complir.
Condició de satisfacció: El tractament de les dades de caràcter personal que farà el sistema respectarà íntegrament la legislació vigent del Reial decret.

## 3. MOCK-UPS ##

![](https://s17.postimg.org/glj5dn8vj/Mock_info_personal.png)
![](https://s17.postimg.org/jd2f497e7/Mock_up_mapa.png)
![](https://s17.postimg.org/h9rzwl7lb/Mock_Servei_d_esports.png)
