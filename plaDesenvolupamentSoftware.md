# Erasmus UPC - PLA DE DESENVOLUPAMENT DE SOFTWARE #



## 1. ORGANITZACIÓ I EQUIP ##

L'equip de desenvolupament del software està format per sis treballadors organitzats de la següent manera:

1. *Analista de sistemes*: Serà l'encargat del càlcul algorismic que suposa l'aplicació
2. *Arquitecte del software*: Encargat de dissenyar les estructures de dades òptimes per accedir a la informació
3. *Analista programador*: Encargat d'especificar les taules UML fent una abstracció del problema.
4. *Programador*: Escriure el codi resultant d'implementar les taules UML
5. *Tester*: Realitzar els jocs de prova complexos per tal de comprovar que l'aplicació relament funciona.
6. *Gestor de projecte* Encargat de parlar amb el client i anar informant dels avanços del projecte, així com donar suport i guía al desenvolupament del projecte.

Veient l'exel podem veure que els programadors tindràn moltes hores de treball i a més, aquestes estaran concentrades en un curt període de temps, fet que obliga que en necessitem dos.
Els altres rols tenen el temps de treball més repartit, per tant és suficient un treballador per rol.

## 2. PLA DE PROJECTE ##

### 2.1. Estimació d'esforç ###

Per realitzar els càlculs hem utilitzat l'eina Excel. Per calcular les hores partim de l'UCP i del PF. S'ha de dir que és una estimació. El PF és el factor d'esforç per punt de cas d'ús. I com bé veurem més endavant el FP és de 15 hores/persona.

L'UUCW fa referència als casos d'ús del sistema i al pes d'aquests, així com els esdeveniments externs.

![](https://s18.postimg.org/p8fg68ry1/Captura_de_pantalla_2016_11_03_23_06_08.png)

L'UAW fa referència als actors, qualsevol entitat externa que interaccioni amb ells, tenint en compte el seu pes. 

![](https://s18.postimg.org/98wsmovw9/Captura_de_pantalla_2016_11_03_23_06_19.png
)

El TCF fa referència a la complexitat tècnica, on cal fixar el pes i prioritat. És un percentatge que es calcula com la suma dels pesos per la prioritat d'aquests dividit per 100, més un factor 0,6.

![](https://s18.postimg.org/hd0bdosxl/Captura_de_pantalla_2016_11_03_23_06_41.png
)

Per últim tenim l'EFC, que són els factors d'entorn, que són factors relacionals al projecte i context, però que poden influir. Es calcula com la suma dels pesos dels factors per la seva avaluació, multiplicat per un factor de -0,03 i tot sumat a 1,4.

![](https://s18.postimg.org/gm7l7wqk9/Captura_de_pantalla_2016_11_03_23_07_12.jpg
)

Hem consultat les taules per saber els pesos de cada cosa, així com tot el necessari per fer l'estudi de l'UCP.

Aquest càlcul queda de la següent manera:

UCP = (UUCW + UAW) * TCF * ECF = 142,59685


### 2.2. Estimació de cost ###

![](https://s22.postimg.org/bu00xqqyp/pressupost.jpg)

Com podem veure el cost total del projecte és de 45.410,71€

*Els pagaments mensuals estan calculats en 14 pagues anuals.


## 3. PLA DE FASES ##

Seguidament podem veure la taula del pla de fases.

![](https://s11.postimg.org/oeo5buvyr/Captura_de_pantalla_2016_11_04_00_40_41.png)

### Inception ###
__Objectius__:

Iteració I1

1. *Definir la visió del projecte:* explicar quin problema volem resoldre, parts interesades, descripció del producte, dependències, etc.
2. *Determinar abast del projecte*. Definir totes les funcionalitats del projecte i descobrir quines són les que són més importants pél client.
3. *Crear el cas de Negosi*. Definir els objectius i restriccions del producte i realitzar una previsió financera.
4. *Definir l'arquitectura candidata*. Pensar i buscar les millors tecnologies per implementar el projecte.
5. *Crear el pla de desenvolupament*. 


__Entregables importants__:

1. *Visió del producte*
2. *Cas de Negosi*
3. *Pla de desenvolupament*

__Número de iteracions__:

Veient les entregues podem veure que amb una iteracció seria suficient ja que són entregues no repetibles.

__Altres punts__:

El projecte comença el dia 9 de gener de 2017 i acaba el 14 de febrer de 2017, ambdós inclosos. Això suposen 27 dies laborables i són 214 hores treballades. L'esforç d'aquesta fase es de 107.

### Elaboration ###

__Objectius__:

Iteració E1

1. *Instal.lar i provar arquitectura*: Comprovar que l'arquitetura triada ens serà útil.
2. *Validar detalls dels requisits*: Revisar els casos d'ús amb el client (poden canviar després de l'estudi de marqueting o el calendari)
3. *Implementar casos d’ús prioritaris*: Implementar aquells casos d'us que estiguin desidits i siguin més importants pél client.

Iteració E2

4. *Mitigar riscos arquitectònics*: Arreglar els posibles problemes que ens ha donat l'arquitectura.
5. *Completar la prova de l’arquitectura*
6. *Implementar casos d’ús addicionals*

__Entregables importants__:

1. *Casos d'ús prioritaris*:
2. *Casos d'ús addicionals*

__Número de iteracions__:

Veient les entregues podem veure que amb una iteració bastaria ja que no hi ha posible revisió (no es utilitzat per l'usuari estandard)

__Altres punts__:

La fase d'elaboration comença el dia 15 de febrer de 2017 i acaba el dia 7 de juny de 2017. Això suposen 81 dies laborables i són 642 hores treballades. L'esforç d'aquesta fase es de 428.


### Construction ###

__Objectius__:

Iteració C1

1. *Descriure casos d’ús addicionals*: En cas de voler tenir algún altre cas d'ús es podria mirar si es viable implementar-lo.
2. *Dissenyar subsistemes addicionals*: En aquest punt es podria implementar la part administrativa de l'aplicació.
3. *Implementar casos d’ús i subsist*: Anar acabant tots els casos d'ús.
4. *Integrar el producte i validar l’estat*: Testajar que els casos d'ús implementats estàn funcionant com esperam.

Iteració C2
 
5. *ídem*

Iteració C3 

6. Ídem

6. *Planificar versió beta i suport usuari*

__Entregables importants__:

1. *Part adminstrativa*: Imporant que els adminstradors puguin començar a guardar dades a la base de dades
2. *En la primera iteració hi ha que tenir tots els casos d'ús especificats i gran part d'ells programats*
3. *En la segona iteració hi pot haver canvis de disseny però s'espera que es corregeixin aquests errors i tots els casos d'ús agradin al client*
4. *En la tercera tots els casos d'ús han d'estar programats*


__Número de iteracions__:

Veient les entregues podem veure que amb dues iteracions seria suficient ja que no hi ha posible revisió (no es utilitzat per l'usuari estandard). Pero afegim un tercera per assegurarnos de que no hi haurà problemes.

__Altres punts__:

La fase de construction comença el dia 8 de juny de 2017 i acaba el dia 11 de desembre de 2017. Això suposen 134 dies laborables i són 1069 hores treballades. L'esforç d'aquesta fase es de 1390.

### Transition ###

__Objectius__:

T1

1. *Desplegar beta en client*: El client ja podrà utilitzar la aplicació per tal de comprovar que tot funciona com ell espera.
2. *Obtenir i processar feedback*
3. *Finalitzar suport usuari*
4. *Entrega a client*

__Entregables importants__:

1. *Entrega definitiva*: Si no hi ha hagut massa problemes durant les fases, s'hauria de poder entregar el producte al client

__Número de iteracions__:

Si totes les fases han funcionat com s'esperava no hi hauria d'haver més d'una única iteració.

__Altres punts__:

La fase de construction comença el dia 12 de desembre de 2017 i acaba el dia 19 de gener de 2018. Això suposen 27 dies laborables i són 214 hores treballades. L'esforç d'aquesta fase es de 214.
