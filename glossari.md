# Erasmus UPC - GLOSSARI #

- E-Secretaria: És una eina online que ofereix la UPC on els alumnes es poden matricular a assignatures, fer tramits i descarregar-se documentació relacionada amb la seva situació acadèmica.

- Guia docent: La guia docent és un subapartat de les assignatures de les universitats on s'explica: les competències i objectius, els continguts i activitats, la docència i avaluació, els recursos, i les capacitats prèvies de cada una de les assignatures de les diferents carreres.

- SALSA'M: Institució de la UPC que ajuda a integrar socialment a les persones que realitzen l'estada internacional a la seva universitat.

- Servei d'Esports UPC: Servei ofert per la universitat on l'alumnat, PDI, PAS i externs poden fer ús d'instal·lacions esportives.

- TMB (Transports Metropolitans de Barcelona): TMB és el principal operador de transport públic de Catalunya i tot un referent d'empresa de transport i mobilitat ciutadana a Europa i a tot el món.




